### Begin Release Tagging Vars

LATEST_TAG := $(shell git describe --tags --abbrev=0)
TAGGED_RELEASE ?= False

RELEASE_TYPE ?= patch

CURRENT_VERSION := $(shell git describe --tag --abbrev=0)
LATEST_SHA := $(shell git rev-parse origin/master)

ifndef CURRENT_VERSION
	CURRENT_VERSION := 0.0.0
endif

NEXT_VERSION ?= $(shell docker run --rm alpine/semver semver -c -i $(RELEASE_TYPE) $(CURRENT_VERSION))

### End of Release Tagging Vars

### Begining Docker Vars
DOCKER_IMAGE="version-bump"
### End Docker Vars


.PHONEY: all create_tag print_input docker_build docker_list_images

all:
ifeq ($(TAGGED_RELEASE), True)
	@echo "\n\nChecking Tagged Release\n\n"
	$(MAKE) create_tag
else
	@echo "Not executing Tagged Release: "$(NEXT_VERSION)
	$(MAKE) docker_build
endif

create_tag:
	@echo "Creating Initial Annotated Tag: "
	@git checkout master
	@git tag -a -m $(LATEST_SHA) $(NEXT_VERSION)
	$(MAKE) docker_build
	@git push --tags

print_inputs:
	@echo "Current Version: "$(CURRENT_VERSION)
	@echo "Next Version: "$(NEXT_VERSION)

docker_list_images:
	docker images $(DOCKER_IMAGE) -a

docker_build:
	docker build -t $(DOCKER_IMAGE):$(NEXT_VERSION) .
