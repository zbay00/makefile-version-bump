FROM alpine:latest

RUN apk add --no-cache alpine-sdk git

CMD ["/bin/bash", "echo Hello-World"]
